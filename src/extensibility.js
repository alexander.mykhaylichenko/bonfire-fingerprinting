function surveyFingerprint(config) {
    if (!objectValid(config)) return false;

    // set default config variables if missing
    if (isNaN(config.cookieExpiresAfterDays)) config.cookieExpiresAfterDays = 7; // default 7 days
    if (!config.cookieName) config.cookieName = 'mcxSurveyFingerprint';
    if (!config.fieldTags) config.fieldTags = {};
    if (config.onlyOnSurveyFinish === undefined) config.onlyOnSurveyFinish = true;
    
    $(document).ready(function() { // main
        // get all fingerprint question fields on this page
        var fields = Object.keys(config.fieldTags).reduce(function(acc, field) {
            var fieldEl = $('#' + config.fieldTags[field] + '_question');
            if (fieldEl.length)  acc[field] = fieldEl;
            return acc;
        }, {});

        // hide fields
        if (config.hideFields === undefined || config.hideFields) {
            Object.keys(fields).forEach(function(field) {
                fields[field].hide();
            });
        }

        var now = new Date();
        var surveyCode = $('input[type="hidden"][name="id"]').val();
        var sessionID = $('input[type="hidden"][name="respondent"]').val();
        if (!sessionID) return true; // no error here, no sessionID simply means this is not a question page

        try {
            // check if cookies enabled
            if ((document.cookie = 'Session=test').indexOf.call(document.cookie, 'Session') == -1) throw 'Security Error. Unable to access cookies';

            var allCookieValues = {}; // stores the whole cookie data
            var surveyCookieValues = {}; // stores the cookie data only for this survey
            var cookieMatch = document.cookie.match(new RegExp('(^| )' + config.cookieName + '=([^;]+)'));
            if (cookieMatch && cookieMatch.length > 1) { // already has cookie
                allCookieValues = JSON.parse(decodeURIComponent(cookieMatch[2]));
            }
            if (objectValid(allCookieValues)) {
                surveyCookieValues = allCookieValues[surveyCode];
            }
            var expiry = new Date(surveyCookieValues && surveyCookieValues.cookieExpiryDate);
            if (!objectValid(surveyCookieValues) || 
                !surveyCookieValues.GUID || isNaN(surveyCookieValues.numberOfVisits) || // most basic values don't exist / are invalid
                (isNaN(expiry) || expiry < new Date()) // cookie has invalid expiry date or has already expired
            ) {
                surveyCookieValues = {
                    GUID: generateGUID(),
                    numberOfVisits: 0
                };
            }
            
            if (sessionID !== surveyCookieValues.sessionID) { // only update cookie if session is new
                surveyCookieValues.sessionID = sessionID; // update sessionID for next fingerprint check
                surveyCookieValues.numberOfVisits += 1;
                surveyCookieValues.lastSetDate = now.toUTCString();

                // check if moreThanXVisits exists and set for X visits
                for (field in config.fieldTags) {
                    if (config.fieldTags.hasOwnProperty(field)) {
                        var match = field.match(/moreThan(\d+)Visits/);
                        if (match && match.length == 2) {
                            var visitNum = parseInt(match[1]) || 3; // number greater than 0
                            surveyCookieValues[field] = surveyCookieValues.numberOfVisits > visitNum? 1 : 0;

                            break;
                        }
                    }
                }
                //surveyCookieValues.moreThan3Visits = surveyCookieValues.numberOfVisits > 3? 1 : 0;

                // only update on thank you page (if set)
                if (
                    config.onlyOnSurveyFinish &&
                    window.navigationState &&
                    !window.navigationState.isExitPage
                ) {
                    // set values of fields    
                    Object.keys(fields).forEach(function(fieldName) {
                        populateField(fields[fieldName], surveyCookieValues[fieldName]);
                    });
                    
                    return true;
                }

                if (!surveyCookieValues.cookieExpiryDate) {
                    now.setDate(now.getDate() + config.cookieExpiresAfterDays);
                    surveyCookieValues.cookieExpiryDate = now.toUTCString();
                }

                allCookieValues[surveyCode] = surveyCookieValues;

                // save cookie
                document.cookie =
                    config.cookieName + "=" + encodeURIComponent(JSON.stringify(allCookieValues)) + 
                    '; expires=' + allCookieValues[surveyCode].cookieExpiryDate + 
                    '; path=/';
            }

        } catch (e) {
            if (config.fieldTags.errorLog && fields.errorLog) {                
                if (e instanceof SyntaxError) {
                    populateField(fields.errorLog, 'Syntax Error. Problem parsing cookie data: ' + cookieMatch[2]);
                } else {
                    populateField(fields.errorLog, e);
                }
            }

            console.error(e);

            return false; // returns false on error
        }
        
        // set values of fields    
        Object.keys(fields).forEach(function(fieldName) {
            populateField(fields[fieldName], surveyCookieValues[fieldName]);
        });

        return true; // ran successfully, but not necessarily changed field values
    });

    // Platform dashboard method of generating a GUID
    function generateGUID() {
        return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(e) {
            var t = 16 * Math.random() | 0;
            return ("x" == e ? t : 3 & t | 8).toString(16);
        });
    }

    // populate Platform question
    function populateField(field, value) {
        field.find('input:text, textarea, input[value="' + value + '"], option[value="' + value + '"]') // find input object
            .prop({'checked': true, 'selected': true}) // select dropdown or radio
            .val(function(i, v) {return v || value}); // populate textbox
    }

    // Object sanity check
    function objectValid(obj) {
        return !$.isEmptyObject(obj) && obj.constructor === Object;
    }
}